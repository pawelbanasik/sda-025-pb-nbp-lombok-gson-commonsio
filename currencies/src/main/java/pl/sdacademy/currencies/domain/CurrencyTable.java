package pl.sdacademy.currencies.domain;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CurrencyTable {

    private String table;

    @SerializedName("no")
    private String number;

    private String effectiveDate;

    @SerializedName("rates")
    private List<CurrencyRate> currencies;

    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        this.table = table;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(String effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public List<CurrencyRate> getCurrencies() {
        return currencies;
    }

    public void setCurrencies(List<CurrencyRate> currencies) {
        this.currencies = currencies;
    }
}
